class Point:
    def move(self):
        print("move")

    def draw(self):
        print("draw")


point1 = Point()
point1.draw()
point1.x = 10
point1.y = 20
print(point1.x)


point2 = Point()
point2.move()
point2.x = 2
point2.y = 4
print(point2.y)

