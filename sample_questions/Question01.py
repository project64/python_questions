#Question 1 for concanating the variables and length of string
first_name='Priya'
last_name='priyadarshi'
message= f'hi {first_name} {last_name}'
print(message.upper())
print(message.lower())
print(message.title())

# find the length of a string
first_name='Priya'
last_name='priyadarshi'
message= f'hi {first_name} {last_name}'
print(len(message))
# to find the charactor and replace the charactor
course = 'pythe is very easy language'
print(course.find('v'))
print(course.replace('v','m'))

#If statement programs
is_hot= True
is_cold= False

if is_hot:
    print("it's sunny day" )
    print('take sun bath')
elif is_cold:
    print("it's cold day")
    print('wear warm cloaths')
else:
    print("don't know")
print('enjoy your day')